#!/bin/bash

AM_BASE=${AM_BASE:-$(dirname $(readlink -f $0))}
APT_DIST=${APT_DIST:-"stretch stretch-updates"}
APT_SECS=${APT_SECS:-"main contrib non-free"}

generate_apt_url() {
    APT_URL=""
    for j in ${APT_DIST[@]} ; do
        APT_URL+="deb file:${AM_BASE}/mirror/debian ${j} ${AM_SECS}\n"
    done
    echo -e ${APT_URL}
}

while [ x"$1" != x ]
do
    case "$1" in
        -h|--help)
            echo "$0 [mirror-path]"
            exit 0
            ;;
        *)
            AM_BASE=$1
            ;;
    esac
    shift 1
done

generate_apt_url
