#!/usr/bin/env python

def qe_nix(fn="nix-qas.txt"):
    db_np = dict()
    with open(fn, "r") as f:
        for l in f.readlines():
            vp = l.rfind('-')
            pn = l[5:vp]
            pv = l[vp+1:].strip()
            ps = l[:3]
            if pn in db_np:
                db_np[pn].append([pv, ps])
            else:
                db_np[pn] = [[pv, ps],]
    return db_np

if __name__ == '__main__':
    import sys
    from pprint import pprint

    if not sys.argv[1:]:
        print("Usage: {} [filename] [package]".format(sys.argv[0]))
        sys.exit(0)

    db_np = qe_nix(sys.argv[1])
    
    if not sys.argv[2:]:
        for k in db_np:
            print(k, db_np[k])
    else:
        for p in sys.argv[2:]:
            print("package: {}".format(p))
            if p in db_np:
                pprint(db_np[p])
            else:
                print("  Not found!")

