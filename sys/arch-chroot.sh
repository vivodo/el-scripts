#!/bin/bash

if  [[ $EUID != 0 ]]; then
    "Please run as root!"
    exit 0
fi

####
VLIVEARCH_PATH=${1:-/media/vivodo/VLiveArch}

if [[ ! -d ${VLIVEARCH_PATH} ]]; then
    "${VLIVEARCH_PATH}: non exist path !"
    exit 0
fi

####
# https://wiki.archlinux.org/index.php/Chroot#Using_chroot
# cd /location/of/new/root
# mount -t proc /proc proc/
# mount --rbind /sys sys/
# mount --rbind /dev dev/
# mount --rbind /run run/
# cp /etc/resolv.conf etc/resolv.conf
# chroot /location/of/new/root /bin/bash

# Debian: /dev/shm => /run/shm

mount -t proc /proc ${VLIVEARCH_PATH}/proc/
mount --bind /sys ${VLIVEARCH_PATH}/sys/
mount --bind /dev ${VLIVEARCH_PATH}/dev/
mount --bind /run ${VLIVEARCH_PATH}/run/
mount --bind /tmp ${VLIVEARCH_PATH}/tmp/
cp /etc/resolv.conf ${VLIVEARCH_PATH}/etc/resolv.conf

chroot ${VLIVEARCH_PATH} /bin/bash

# after exit
sleep 1
umount ${VLIVEARCH_PATH}/tmp/
umount ${VLIVEARCH_PATH}/run/
umount ${VLIVEARCH_PATH}/dev/
umount ${VLIVEARCH_PATH}/sys/
umount ${VLIVEARCH_PATH}/proc/
