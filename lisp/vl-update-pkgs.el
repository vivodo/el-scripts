#!/bin/sh
":"; # -*- mode: emacs-lisp; lexical-binding: t; -*-
":"; exec emacs --script "$0" "$@"

;; Copyright (C) 2018  Vivodo Lio

;; Author: Vivodo Lio <Exaos.Lee@gmail.com>
;; Keywords: lisp

;; (message "Hello world")
;; (message "Hello: %S" argv)

(require 'package); activate all the packages
(add-to-list 'package-archives
             '("org-cn"     . "http://elpa.emacs-china.org/org/") t)
(add-to-list 'package-archives
             '("melpa-cn"     . "http://elpa.emacs-china.org/melpa/") t)

(package-initialize)

(setq vl-path-lisp-scripts
      (expand-file-name  "Laborejo/scripts/lisp/" (getenv "HOME" )))
(add-to-list 'load-path vl-path-lisp-scripts)
(require 'auto-package-update)

(auto-package-update-maybe)
