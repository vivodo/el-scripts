#!/bin/bash
# -*- mode: shell-script; coding: utf-8; -*-
# by Exaos Lee (exaos.lee@gmail.com)

if [[ "x$@" == "x" ]]; then
    echo "Usage: $0 <file>"
    exit 0
fi

file_copyright=$(exiftool "-*copyright*" $1)
if [[ ! -z "${file_copyright}" ]]; then
    echo "$1: ${file_copyright}"
fi
