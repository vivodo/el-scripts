#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
@Exaos
'''

import json, yaml, io

def yaml2json(fn, fn_out):
    with io.open(fn, "r", encoding='utf-8') as y_fin:
        json.dump(yaml.load(y_fin), open(fn_out, "w"),
                  indent=2, ensure_ascii=False)

def json2yaml(fn, fn_out):
    with io.open(fn, "r", encoding='utf-8') as js_fin:
        yaml.safe_dump(json.load(js_fin), stream=open(fn_out, "w"),
                       encoding='utf-8', allow_unicode=True)

def main():
    import sys
    if len(sys.argv) < 2:
        print("Usage: yaml2json.py [filename.yaml|yml]")
        exit(0)
    is_reverse = False
    if "-r" in sys.argv:
        is_reverse = True
        sys.argv.remove("-r")
    fn = sys.argv[1]
    if is_reverse:
        if fn.endswith(".json"):
            fn_out = fn.replace(".json", ".yaml")
        else:
            fn_out = fn + ".yaml"
        json2yaml(fn, fn_out)
    else:
        if fn.endswith(".yaml"):
            fn_out = fn.replace(".yaml", ".json")
        elif fn.endswith(".yml"):
            fn_out = fn.replace(".yml", ".json")
        else:
            fn_out = fn + ".json"
        yaml2json(fn, fn_out)

if __name__ == "__main__":
    main()
