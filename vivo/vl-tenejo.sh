#!/bin/bash
# -*- mode: shell-script; coding: utf-8; -*-
# by Exaos Lee (exaos.lee@gmail.com)

VD_TENEJO=${VD_TENEJO:-~/Tenejo}
VD_TENEJO_MDISK=${VD_TENEJO_MDISK:-VTENEJ5T}
VD_TENEJO_BOTS=${VD_TENEJO_BOTS:-"ejo kolektoj gr-datumoj personaj"}

depot_status() {
    cd ${VD_TENEJO}/$1
    if [ "$(git status --porcelain)" ]; then
        echo "==> [${VD_TENEJO}: $1] \$ Status:"
        git status
    fi
    cd ${OLDPWD}
}

depot_sync() {
    echo "==> [${VD_TENEJO}: $1] \$ sync with $2 ..."
    cd ${VD_TENEJO}/$1
    git-annex sync $2
    cd ${OLDPWD}
}

# depot_copy() {
#     echo "==> [${VD_TENEJO}: $1] \$ Copy data to depot $2 ..."
#     cd ${VD_TENEJO}/$1
#     git annex copy --all --auto --to $2 --not --in $2
#     cd ${OLDPWD}
# }

depot_cmd() {
    echo "==> [${VD_TENEJO}: $1] \$ $2"
    local BASE=${1}
    shift 1
    local CMD=${1:-git}
    shift 1
    cd ${VD_TENEJO}/${BASE}
    echo "[base:${BASE}] \$ ${CMD} ${@}"
    ${CMD} "${@}"
    cd ${OLDPWD}
}

depot_git_cmd() {
    local BASE=${1}
    shift 1
    local GA_CMD=${@}
    depot_cmd ${BASE} git ${GA_CMD}
}

depot_git_annex_cmd() {
    local BASE=${1}
    shift 1
    local GA_CMD=${@}
    depot_cmd ${BASE} git annex ${GA_CMD}
}

status_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_status $d
    done
}

sync_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_sync $d $1
    done
}

copy_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_copy $d $1
    done
}

cmd_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_cmd $d $@
    done
}

git_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_git_cmd $d $@
    done
}

git_annex_all() {
    for d in ${VD_TENEJO_BOTS} ; do
        depot_git_annex_cmd $d $@
    done
}

usage() {
    echo "Usage: $0 <cmd> [<parameters>]"
    echo "Depot base path: ${VD_TENEJO}"
    echo "Archive disk:    ${VD_TENEJO_MDISK}"
    echo "Handling depots: ${VD_TENEJO_BOTS}"
    echo "Commands: status sync <cmd...>"
    echo "Git command: git|g <cmd...>"
    echo "Git annex commands: ga|annex <cmd...>"
}

if [[ -z "$1" ]]; then
    usage
    exit 0
fi

case "$1" in
    s | st | status)
        status_all
        ;;
    sync)
        DEST=${2:-${VD_TENEJO_MDISK}}
        sync_all ${DEST}
        ;;
    # cp | copy)
    #     DEST=${2:-${VD_TENEJO_MDISK}}
    #     copy_all ${DEST}
    #     ;;
    g | git)
        shift 1
        git_all $@
        ;;
    ga | annex)
        shift 1
        git_annex_all $@
        ;;
    *)
        cmd_all $@
        ;;
esac
