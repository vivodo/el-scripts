#!/bin/bash

[[ -f ${VL_VOJOJ_SET} ]] && source ${VL_VOJOJ_SET}
if [[ ${#VL_VOJOJ[@]} -eq 0 ]] ; then
    echo "Environment \"VL_VOJOJ\" is undefined."
    exit 0
fi

##########
OS_TYPE=`uname -s | tr '[A-Z]' '[a-z]'`
VD_LABOREJO=${VD_LABOREJO:-$HOME/Laborejo}
VD_DEPOJ_DEFAULT=${HOME}/Tenejo/deponejoj/laborejo
VD_DEPOJ=${VD_DEPOJ:-${VD_DEPOJ_DEFAULT}}

####
abs_path () {
    if [ -d $1 ]; then
        TMPDIR=$1
    else
        TMPDIR=`dirname $1`
    fi
    PSTR=`cd ${TMPDIR} && pwd -P`
    echo $PSTR
}

rel_path () {
    if [ -z "$2" ]; then
        REL_PATH=${VD_LABOREJO}
    else
        REL_PATH=$2
    fi
    printf "%s" $(realpath --relative-base=${REL_PATH} $1)
}

## list all repos
repos_list () {
    echo "Vojo de mia laborejo: ${VD_LABOREJO}"
    echo "----------------------------------------------------------"
    for key in ${!VL_VOJOJ[@]} ; do
        printf "(%-22s . \"%s\")\n" $key $(rel_path ${VL_VOJOJ[$key]})
    done | sort
}

#### Repositories monitered
sync_repo () {
    [[ "$#" -ne 2 ]] && exit 1

    echo "Working on $1 ..."
    echo "==============================="
    cd "$1"
    # First: pull
    echo "Fetch from $2 ..."
    git fetch -t "$2"
    echo "Pull recent updates from $2 ..."
    git pull "$2"
    # Then: push
    echo "-------------------------------"
    echo "Push local updates to $2 ..."
    git push --all "$2"
    echo "Push tags to $2 ..."
    git push --tags "$2"
    echo
}

sync_host () {
    if [ "$1" != "path" ]; then
        if [ "$2" ]; then
            VD_DEPOJ=$2
        fi
        RREPO_ROOT="${1}:${VD_DEPOJ}"
    else
        if [ -z "$2" ] ; then
            lrpath=${VD_DEPOJ}
        else
            lrpath="$2"
        fi
        RREPO_ROOT=`abs_path $lrpath`
    fi
    if [ -z "${RREPO_ROOT}" ]; then
        echo "WARNING: Incorrect repositories root path!"
        exit 1
    fi
    echo "+-------------------------------------------------------+"
    echo "|  Syncing local repositories with ${RREPO_ROOT} ..."
    echo "+-------------------------------------------------------+"

    for key in "${!VL_VOJOJ[@]}" ; do
        sync_repo ${VL_VOJOJ[$key]} ${RREPO_ROOT}/${key}.git
    done
}

## git on repos
repos_status () {
    echo "Vojo de mia laborejo: ${VD_LABOREJO}"
    for key in ${!VL_VOJOJ[@]} ; do
        cd ${VL_VOJOJ[$key]}
        if [ "$(git status -s)" ]; then
            echo "----------------------------------------------------------"
            printf "(%s) %s\n" $key $(rel_path ${VL_VOJOJ[$key]})
            git status
        fi
    done
}

repos_git () {
    for key in ${!VL_VOJOJ[@]} ; do
        echo "=================================================="
        printf "[%s](%s):\n" "$key" $(rel_path ${VL_VOJOJ[$key]})
        echo "Running \"git $@\" ..."
        echo "--------------------------------------------------"
        cd ${VL_VOJOJ[$key]}
        if [[ -z "$1" ]] ; then
            git status
        else
            git $@
        fi
        echo
    done
}

repos_cmd () {
    for key in ${!VL_VOJOJ[@]} ; do
        echo "=================================================="
        printf "[%s](%s):\n" "$key" $(rel_path ${VL_VOJOJ[$key]})
        echo "Running \"$@\" ..."
        cd ${VL_VOJOJ[$key]}
        echo "--------------------------------------------------"
        if [[ -z "$1" ]] ; then
            git status
        else
            $@
        fi
        echo
    done
}

##########################################################
usage () {
    echo "Usage: $0 <cmd> [<parameters>]"
    cat <<EOF
Commands:
  + <l|list>         --  list monitored repositories
  + <s|st|status>    --  check status of repositories
  + <p|path> [path]
                     --  sync repos with local repositories,
                         omitting path means syncing with default path.
  + <r|remote> <[user@]hostname>
                     --  sync repos with [user@]hostname
  + [g|git] <cmd>    --  git commands on repos
EOF
}

# __main__
if [[ -z "$1" ]]; then
    usage
    exit 0
fi

case "$1" in
    l | list)
        repos_list
        ;;
    s | st | status)
        repos_status
        ;;
    p | path)
        shift
        sync_host path $@
        ;;
    r | remote)
        shift
        sync_host $@
        ;;
    g | git)
        shift
        repos_git $@
        ;;
    *)
        repos_cmd $@
        ;;
esac
